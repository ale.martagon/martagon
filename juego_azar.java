import java.util.Scanner;
import java.lang.Math;

//COMENTARIO PRUEBA

public class juego_azar {
	
	public static void main(String[] args) {
	
		Scanner numero = new Scanner(System.in);
		double saldo=10;
		int numMenu;
		int numLoteria;
		
		do{
			System.out.println("1. Ver saldo disponible \n2. Aumentar saldo disponible\n3. Loteria (3 euros) \n4. Euromillon (2,5 euros) \n5. Salir");
			numMenu = numero.nextInt();
			
		
			switch(numMenu) {
				
				//ver saldo
				case 1:
					System.out.println("\nTiene: "+saldo+" euros de saldo\n");
					
				break;
				
				//aumentar saldo 
				case 2: 
						System.out.println("Introduce el saldo que quieras aumentar, si lo pones en negativo, igualmente se aumentara");
						saldo = Math.abs(numero.nextInt())+saldo;
						System.out.println("Saldo actualizado. Tienes= "+saldo);
					
				break;
				
				//comprar y comprobar loteria
				case 3:
					if (saldo>=3){
						System.out.println("\n////LOTERIA////\n");
						
						//llamaos a comprobarPremios
						saldo=comprobarPremios(comprar(), boletoGanador(), saldo);
						
						//restamos saldo por jugar
						saldo=saldo-3;
					}else System.out.println("No tienes saldo suficiente, use la segunda opcion del menu");
					
				break;
				
				//comprar euromillon
				case 4:
					if (saldo>=2.5){
						System.out.println("////EUROMILLON////\n");
						
						

						
						saldo=saldo-2.5;
					}else System.out.println("No tienes saldo suficiente, use la segunda opcion del menu");
					
				break;
				
				default:
					System.out.println("No es un numero del menu");
			}
		}while (numMenu!=5);
	}
		
	public static int comprar(){
		int numLoteria=-1;
		int tipoBoleto;
		Scanner numero = new Scanner(System.in);
			do{
				System.out.println("1. Comprar un numero aleatorio  \n2. Elegir numero");
				tipoBoleto=numero.nextInt();
			}while(tipoBoleto!=1 && tipoBoleto!=2);
			
			if (tipoBoleto==1) {
				numLoteria = ((int) (Math.floor((100000)* Math.random())));
				System.out.println("Tu numero de loteria es: "+numLoteria+"\n");
			} else if (tipoBoleto==2){
				//introducimos el numero de boleto que queramos
				System.out.println("Introduce el numero de boleto que desee");
				do{
					System.out.println("NUMERO DEL 0 AL 99999");
					numLoteria=numero.nextInt();
				}while (numLoteria<=0 || numLoteria>=99999);
			}	
		return numLoteria;
	}
	
	public static int boletoGanador (){
		int numGanador;
		
		//conseguimos de forma aleatoria el numero ganador y lo mostramos or pantalla
		System.out.println("Vamos a sortear el numero ganador...");
		numGanador = ((int) (Math.floor((100000)* Math.random())));
		System.out.println("Numero Ganador: "+numGanador);
		
		return numGanador;
	}
	
	public static double comprobarPremios (int boletoComprado, int boletoGanador, double saldo){
		
		//inicializamos un array con el precio de los premios por ganar
		int premios[]={0, 3, 6, 15, 75, 30000};
		//declaramos una variable para recorrer el array de premios
		int numAcertados=5;
		//declaramos una variable para sumar el premio anterior y posterior, que va a parte
		int premioAP=0;
		
		//comprobamos si son igualos numeros, calculando los restos de los numeros dividiendolos entre 10 sucesivamente, hasta que el contador i sea mayor o igual a 10
		for (int i=100000; i>=10; i=i/10){
			
			if(boletoComprado%i == boletoGanador%i){
				break;
			}
			numAcertados--;
			
		}
		
		//comprobamos si es el numero anterior o posterior 
		if(boletoComprado==boletoGanador+1 || boletoComprado==boletoGanador-1){
			premioAP=premioAP+1200;
		}
		
		//le añadimos los premios al saldo ya existente del jugador
		saldo=saldo+(premios[numAcertados]+premioAP);
		System.out.println ("Has ganado: "+(premios[numAcertados]+premioAP)+" euros\n");

		return saldo;
		
	}
}